fn fib(n: u32) -> u32 {
    if n == 1 || n == 2 {
        1
    } else {
        fib(n - 1) + fib(n - 2)
    }
}

fn main() {
    let n = 34;
    println!("The first {} Fibonacci numbers are: ", n);
    for i in 1..n {
        println!("{}", fib(i));
    }
}
